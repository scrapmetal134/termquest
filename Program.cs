﻿using System;
using System.Runtime.InteropServices;
using TermQuest.UI;

namespace TermQuest
{
    class Program
    {
        static void Main(string[] args)
        {
            Screen screen = new Screen(80, 25);
            UIElement text = new TextBox(76, 10, "Hello and welcome to term quest!\n\nThis is a text based adventure meant to\nbe played in an 80 column format.");
            UIElement border1 = new Panel(text);
            BarGraph barGraph = new BarGraph(25, 1, "Health", 100f, 100f);
            UIElement border2 = new Panel("Stats", barGraph);
            TextInput textInput = new TextInput(76, 1);
            UIElement border3 = new Panel("Prompt", textInput);
            Panel border4 = new Panel(border1);

            string command = "";
            do
            {
                screen.ClearScreen();
                if (command == "a")
                {
                    barGraph.SetValue(50f);
                }
                
                Console.WriteLine(border1.Draw(1,1));
                Console.WriteLine(border2.Draw(1,13));
                Console.WriteLine(border3.Draw(1,22));
                textInput.Focus();
                command = Console.ReadLine();
            } while (command != "end");
        }
    }
} 