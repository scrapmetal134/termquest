namespace TermQuest.UI
{
    public interface IFocusable
    {
        void Focus();
    }
}