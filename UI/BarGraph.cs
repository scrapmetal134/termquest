using System;

namespace TermQuest.UI
{
    public class BarGraph : UIElement
    {
        private const char bar = '█';

        private string label;
        private float maxValue;
        private float value;

        public BarGraph(int width, int height, string label, float maxValue, float value) : base(width, height)
        {
            this.label = label;
            this.maxValue = maxValue;
            this.value = value;
        }

        public void SetValue(float value)
        {
            this.value = value;
        }

        public override string Draw(int xPosition, int yPosition)
        {
            string ret = $"\u001b[{yPosition};{xPosition}H" + label + ":";
            int barMaxLength = width - label.Length;
            int barLength = (int)Math.Ceiling(barMaxLength * (value / maxValue));
            
            barLength = Math.Min(barLength, barMaxLength);
            
            for (int i = 1; i < barLength; i++)
            {
                ret += bar;
            }

            return ret;
        }
    }
}