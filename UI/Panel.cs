namespace TermQuest.UI
{
    public class Panel : UIElement
    {
        private const char BoxHor = '─';
        private const char BoxVer = '│';
        private const char BoxUpLeftCorner = '┌';
        private const char BoxUpRightCorner = '┐';
        private const char BoxLoLeftCorner = '└';
        private const char BoxLoRightCorner = '┘';
        private const int TitleXPosition = 3;

        private string title;
        private UIElement child;

        public Panel(int width, int height, string title, UIElement child) : base(width, height)
        {
            this.child = child;
            this.title = title;
        }
        
        public Panel(int width, int height, UIElement child) : base(width, height)
        {
            this.child = child;
            this.title = "";
        }
        
        public Panel(string title, UIElement child) : base(child.width + 4, child.height + 2)
        {
            this.child = child;
            this.title = title;
        }
        
        public Panel(UIElement child) : base(child.width + 4, child.height + 2)
        {
            this.child = child;
            this.title = "";
        }


        public override string Draw(int xPosition, int yPosition)
        {
            string ret = $"\u001b[{yPosition};{xPosition}H";

            for (int r = 0; r < height; r++)
            {
                for (int c = 0; c < width; c++)
                {
                    if (c == 0 && r == 0)
                    {
                        ret += BoxUpLeftCorner;
                    }
                    else if (c == (width - 1) && r == 0)
                    {
                        ret += BoxUpRightCorner;
                    }
                    else if (c == 0 && r == (height - 1))
                    {
                        ret += BoxLoLeftCorner;
                    }
                    else if (c == (width - 1) && r == (height - 1))
                    {
                        ret += BoxLoRightCorner;
                    }
                    else if (c == TitleXPosition && r ==0 && title != "")
                    {
                        ret += title;
                        c += title.Length - 1;
                    }
                    else if (c == 0 || c == (width - 1))
                    {
                        ret += BoxVer;
                    }
                    else if (r == 0 || r == (height - 1))
                    {
                        ret += BoxHor;
                    }
                    else
                    {
                        ret += " ";
                    }
                }
                
                if (r != (height - 1))
                {
                    ret += string.Format("\n\u001b[{0}G", xPosition);
                } 
            }

            return ret + child.Draw(xPosition + 2, yPosition + 1);
        }
    }
}