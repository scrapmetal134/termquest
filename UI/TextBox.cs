using System;

namespace TermQuest.UI
{
    public class TextBox : UIElement
    {
        public string content;

        public TextBox(int width, int height, string content) : base(width, height)
        {
            this.content = content;
        }

        public override string Draw(int xPosition, int yPosition)
        {
            string ret = $"\u001b[{yPosition};{xPosition}H";
            string[] temp;
            
            temp = content.Split('\n');
            for (int i = 0; i < temp.Length; i++)
            {
                ret += temp[i] + $"\u001b[{yPosition + i + 1};{xPosition}H";
            }

            return ret;
        }
    }
}