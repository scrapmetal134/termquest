using System;
namespace TermQuest.UI
{
    public class TextInput : UIElement, IFocusable
    {
        private int focusPositionX;
        private int focusPositionY;
        
        public TextInput(int width, int height) : base(width, height)
        {
        }

        public override string Draw(int xPosition, int yPosition)
        {
            focusPositionX = xPosition;
            focusPositionY = yPosition;
            return $"\u001b[{yPosition};{xPosition}H";
        }

        public void Focus()
        {
            Console.Write($"\u001b[{focusPositionY};{focusPositionX}H");
        }
    }
}