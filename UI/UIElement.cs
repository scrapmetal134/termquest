namespace TermQuest.UI
{
    public abstract class UIElement
    {
        public int width;
        public int height;

        protected UIElement()
        {
            
        }
        
        protected UIElement(int width, int height)
        {
            this.width = width;
            this.height = height;
        }
        
        public abstract string Draw(int xPosition, int yPosition);
    }
}